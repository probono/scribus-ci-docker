FROM ubuntu:18.04
RUN export DEBIAN_FRONTEND=noninteractive && \
apt-get update -yqq && \
apt-get install -y software-properties-common && \
apt-add-repository ppa:beineri/opt-qt-5.11.1-bionic -y && \
apt-get update -yqq && \
apt-get install -y --allow-downgrades --allow-remove-essential --allow-change-held-packages \
cmake libboost-python-dev libcups2-dev libhunspell-dev \
libhyphen-dev liblcms2-dev libpodofo-dev libtiff-dev libxml2-dev \
python-all-dev zlib1g-dev qt511base qt511declarative \
qt511tools libgraphicsmagick++1-dev \
libopenscenegraph-dev libpoppler-dev libpoppler-cpp-dev \
libpoppler-qt5-1 libpoppler-qt5-dev libpoppler73 libpoppler-private-dev \
libcairo2-dev libwpg-dev \
libmspub-dev libcdr-dev libvisio-dev libharfbuzz-dev libharfbuzz-icu0 \
coreutils binutils python-tk wget ccache git \
libgtk2.0-dev \
&& rm -rf /var/lib/apt/lists/*
